
def start_game():
   """Main function, that cal
    others and check the temp of game"""
   field = create_field()
   flag = True #Flag for check the side
   counter = 0 #To check for draw, if the moves are finished
   for x in field:
      print(x) #Show the start condition of matrix
   while conditions_of_win(field)!=True: # Call the Сhecker
      if flag == True:
         mark_the_cell(field,'x')#X move
         flag = False#give the temp to O-player
      elif flag == False:
         mark_the_cell(field,'o')#O move
         flag = True#give the temp to X-player
      for x in field:
         print(x)#Show the present condition 
      counter+=1
      if counter == 9:
         print("Draw")
         break;
   else: print('X wins') if flag == False else print('O wins') #Check who won


def create_field():
   """This function create field 
   3x3 in list of lists format """
   list_ = ['-']*3
   for i in range(3):
      list_[i]=['-']*3
   return list_



def mark_the_cell(field,marker):
   """This function mark the position
    in matrix by certain marker(x,o)"""
   print('Сейчас ходят: ' + marker.upper())
   x,y = input_cord()
   if field[x][y]=='-':
      field[x][y]=marker  
   else: print('Wrong_input')
   return field

def input_cord():
   """This func for inputing
    the cords of the move"""
   hor = input('X:')
   vert = input('Y:')
   if check_the_input(hor,vert): #Check input 
      return int(hor),int(vert)
   else: 
      print('Wrong_input, pls digit 0,1,2')#Call again, if bad-input
      return input_cord()

def check_the_input(hor,vert):
   if hor.isdigit() and vert.isdigit():
      if int(hor) in range(3) and int(vert) in range(3):
         return True
   else: False


def conditions_of_win(field):
   """This func check diaonal,vert 
   and horizontal conditions to win"""
   for x in range(3):
      if field[x][0]==field[x][1]==field[x][2]!='-':#Horizontal Won
         return True
      if field[0][x]==field[1][x]==field[2][x]!='-':#Vertical Won
         return True
   if field[0][0]==field[1][1]==field[2][2]!='-':#diagonal1 Won
      return True
   elif field[2][0]==field[1][1]==field[0][2]!='-':#diagonal2 Won
      return True
   else: return False



start_game()
